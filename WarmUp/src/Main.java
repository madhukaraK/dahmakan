import java.util.stream.Stream;

/**
 * Created by Madhukara on 10/05/18.
 */
public class Main {

    public static void main(String[] args) {
        String inputString = "thisContainsFourWords";
        System.out.println(wordCount(inputString));
    }

    //method to calculate the word count
    static Integer wordCount(String inputString) {
        Integer wordCount = 0;
        if (inputString.length() > 0) {
            //if the inputString lenth is greater than "0" assume there is at least one word exists
            wordCount = wordCount + 1;
            for (int i = 0; i < inputString.length(); i++) {
                if (Character.isUpperCase(inputString.charAt(i))) {
                    wordCount = wordCount + 1;
                }
            }
        }
        return wordCount;
    }

    //single line method to calculate word count.
    static Long wordCountBonus(String inputString) {
        return Stream.of(inputString.split("\\s")).filter(word -> word.equals(word.toUpperCase())).count()+1;
    }
}
