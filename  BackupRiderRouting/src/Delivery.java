import java.time.Instant;

/**
 * Created by Madhukara on 5/10/18.
 */
public interface Delivery {
    String getId();
    /**
     * Returns the instant at which the delivery must be fulfilled.
     */
    Instant getToBeDeliveredAt();
}
