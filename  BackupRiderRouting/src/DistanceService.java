/**
 * Created by Madhukara on 5/10/18.
 */
public interface DistanceService {
    /**
     * Returns the duration in seconds to get from Delivery `a` to Delivery
     `b`.
     * The following always holds: getDistance(a, b) == getDistance(b, a)
     */
    int getDistance(Delivery a, Delivery b);
    /**
     * Returns the duration in seconds to get from the kitchen to the provided
     delivery.
     */
    int getDistanceFromKitchen(Delivery delivery);
}
