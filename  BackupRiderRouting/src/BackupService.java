import java.util.List;

/**
 * Created by Madhukara on 5/10/18.
 */
public interface BackupService {
    /**
     * This method returns an ordered list of deliveries, in the order they
     * should be delivered to. So the first element in the list is the first
     * delivery the rider will visit after leaving the kitchen. The returned
     * deliveries are popped from the internal data structure on return.
     *
     * This method will return null
     * if less than 4 deliveries are currently queued for backup
     * AND
     * none of these deliveries need to leave within the next 5 minutes to
     * be delivered before the delivery’s `toBeDeliveredAt`.
     */
    List<Delivery> deployBackupRider();
    /**
     * Adds the provided delivery to the deliveries eligible for backup.
     */
    void add(Delivery delivery);
}