import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Queue;

/**
 * Created by Madhukara on 5/10/18.
 */
public class BackupServiceImpl implements BackupService {

    List<Delivery> deliveriesToBackup;

    DistanceService distanceService;

    @Override
    public List<Delivery> deployBackupRider() {

        //assume below queue is the list of deliveries to deliver.
        Queue<Delivery> deliveries;
        //check the number of deliveries in queue is less than 4
        if (deliveries.size() < 4){
            return null;
        } else {
            for (Delivery currentDelivery : deliveries){
                Instant instantNow = Instant.now();
                Instant tobeDelivered = currentDelivery.getToBeDeliveredAt().minusSeconds(300);
                //check the delivery tobeDeliveredTime is less than 5 minutes
                if(ChronoUnit.MINUTES.between(instantNow, tobeDelivered) > 5){
                    return null;
                } else {
                    add(currentDelivery);
                }
            }
        }
        return deliveriesToBackup;
    }



    @Override
    public void add(Delivery delivery) {
        int distanceFromKitchen = distanceService.getDistanceFromKitchen(delivery);
        //adding the new delivery according to the distacne from the kitchen
        for (int i=0; i<deliveriesToBackup.size(); i++){
            if (distanceService.getDistanceFromKitchen(deliveriesToBackup.get(i)) > distanceFromKitchen){
                deliveriesToBackup.add(i,delivery);
                break;
            }
        }
        //sort the list according to the distance each delivery
        int remainingSizeOfTheList = deliveriesToBackup.size()-1;
        for(int x = 0; x < (deliveriesToBackup.size()-1); x++) {
            for(int y = 0; y < (remainingSizeOfTheList); y++) {
                if (distanceService.getDistance(deliveriesToBackup.get(y), deliveriesToBackup.get(y+1))
                        > distanceService.getDistance(deliveriesToBackup.get(y), deliveriesToBackup.get(y+2))){
                    deliveriesToBackup.add(y+1, deliveriesToBackup.remove(y+2));
                }
            }
            remainingSizeOfTheList--;
        }
    }
}
