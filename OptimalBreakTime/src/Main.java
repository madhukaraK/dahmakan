import java.util.stream.IntStream;
/**
 * Created by Madhukara on 10/05/18.
 */
public class Main {

    public static void main(String[] args) {
        int a[] = {1, 4, 1, 3};
        System.out.println(optimalBreakTime(a));
    }
    //this method calculate the optimal break time
    static Integer optimalBreakTime (int taskList[]){
        Integer breakTime = null;
        //get the sum of the integer array
        int sum = IntStream.of(taskList).sum();
        int halfSum = sum / 2;
        int n = taskList.length-1;
        int firstHalfSum = 0;
        int secondHalfSum = 0;
        for (int i = 0; i <= n; i++){
            firstHalfSum += taskList[i];
            if (firstHalfSum == halfSum){
                for (int j = i+1; j <= n; j++){
                    secondHalfSum += taskList[j];
                }
                if (firstHalfSum == secondHalfSum){
                    breakTime = i;
                    break;
                }
            }
        }
        return breakTime;
    }
}
